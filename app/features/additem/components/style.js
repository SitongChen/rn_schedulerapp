import {StyleSheet, Dimensions, Platform} from 'react-native';

const styles = StyleSheet.create({
  statusbar:{
    height : (Platform.OS === 'ios') ? 20 : 0,
  },
  container: {
    height: Dimensions.get('window').height - ((Platform.OS === 'ios') ? 20 : 0),
  },
  mainlist: {
    flex: 1,
    backgroundColor: 'powderblue'
  },
  listitem: {
  },
  textinput: {
    height: 40,
    marginLeft: 5,
    marginRight: 5,
    flex:1,
    borderWidth: 2,
    borderColor: "steelblue",
    borderRadius: 10
  },
  textrowview: {
    marginTop: 8,
    marginBottom: 8,
    flexDirection:'row',
    alignItems: "center",
    height: 40
  },
  submitview: {
    marginTop: 8,
    alignItems: "center",
    justifyContent: "center",
    height: 50,
    backgroundColor: "steelblue",
    borderRadius: 12,
    width: Dimensions.get('window').width / 2,
  },
  submitbutton: {
    fontSize: 16,
    textAlignVertical: "center",
    textAlign: "center"
  },


});

export default styles;
