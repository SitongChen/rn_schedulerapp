import React, {Component} from 'react';
import {
  Image,
  Text,
  View,
  TextInput,
  Button,
  Alert,
  Dimensions,
  TouchableHighlight,
} from 'react-native';
import {connect} from "react-redux"
import styles from '../components/style'

const mapStateToProps = state => {
  return {
  }
}

const mapDispatchToProps = dispatch => {
  return {dispatch}
}

class TestAddScreen extends Component{
  static navigationOptions =  ({ navigation }) =>{
    return {
      title: "Edit",
    };
  };
  constructor(props){
    super(props)
    this.submit = this.submit.bind(this)
  }

  submit(){
    if (this.props.type == "modify"){
      this.props.dispatch({})
    }
  }

  render(){
    return(
      <View style={{alignItems: "center"}}>
        <Image
          source={{uri:this.props.navigation.state.params.imgUrl}}
          style={{ height: Dimensions.get('window').width, width: Dimensions.get('window').width}}>
        </Image>
        <View style={styles.textrowview}>
          <Text>Name</Text>
          <TextInput style={styles.textinput}/>
        </View>
        <View style={styles.textrowview}>
          <Text>Description</Text>
          <TextInput
            style={styles.textinput}/>
        </View>

        <TouchableHighlight onPress={this.submit}>
          <View style={styles.submitview} >
            <Text style={styles.submitbutton}>SUBMIT</Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(TestAddScreen);
