import React, {Component} from 'react';
import {
  View,
  FlatList,
  Text,
  Alert,
  Button
} from 'react-native';
import TestListItem from './TestListItem'
import styles from './style'

export default class MainList extends Component{

  render(){
    return (
      <View style={styles.container} >
        <FlatList
          data = {this.props.listData}
          renderItem={({item, index}) =>
            <TestListItem item={item} index={index} itemClick={this.props.itemClick}/>}
        />
      </View>
    );
  }
}
