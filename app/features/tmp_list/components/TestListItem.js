import React, {Component} from 'react';
import {
  Alert,
  View,
  Image,
  Text,
  TouchableHighlight,
  Button
} from 'react-native';

import styles from './style';


class TestListItem extends Component{
  onTap(){
    this.props.itemClick(this.props.item);
  }
  render(){

    return (
      <TouchableHighlight onPress={this.onTap.bind(this)} underlay>

        <View style={styles.listitem}>
          <View style={{flexDirection: 'row',}}>
            <Image
            style={{width: 100, height: 100, margin: 5}}
              source={{uri:this.props.item.imageUrl}} />
            <View style={{ flex: 1, margin: 10}}>
              <Text style={{fontSize: 20, }}> {this.props.item.name} </Text>
              <Text style={{color:'gray', fontSize:15}}>{this.props.item.foodDescription} </Text>
            </View>
          </View>

          <View style={{height:1, backgroundColor:"black"}} />

        </View>
      </ TouchableHighlight>

    );
  }
}
export default TestListItem;
