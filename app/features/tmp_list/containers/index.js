import React, {Component} from 'react';
import {
  View,
  Image,
  Text,
  Button,
} from 'react-native';
import {connect} from 'react-redux'

import MainList from '../components/MainList'
import listData from '../components/tmpData'
import styles from '../components/style'

const defaultImgUrl = "https://www.vetmed.wisc.edu/wp-content/uploads/2016/10/default.jpg";
var delegate;

const mapStateToProps = state => {
  return {
    list : state.list
  }
}

const mapDispatchToProps = dispatch => {
  return {dispatch}
}

class TestListScreen extends Component{
  static navigationOptions = ({ navigation }) => {
        return {
            title: 'FEED',
            headerRight: (
              <Button
                  title='Add'
                  onPress={ () => this.nav() }
                  backgroundColor= "rgba(0,0,0,0)"
                  color="rgba(0,122,255,1)"
              />              ),
            tabBarIcon: ({ tintColor}) => {
                return <Icon name = "favorite" size={26} color={tintColor} />;
            }
        }
  };
  constructor(props){
    super(props)
    this.pushEditScreen = this.pushEditScreen.bind(this)
    this.nav = this.nav.bind(this)
  }
  componentDidMount(){
    delegate = this;
  }
  nav(){
    this.props.navigation.navigate('Modify', {callback: this.callbackFromChildScreen.bind(this), imgUrl: defaultImgUrl, name: '', description: ''});
  }

  callbackFromChildScreen(data){
    console.log("Call from child screen.");
    console.log(data);
    this.setState( previousState => {
      return {flatListData : previousState.flatListData.concat({
        "key": data.key,
        "name": data.name,
        "imageUrl": data.url,
        "foodDescription": data.description,
      })};
    });
  }
  pushEditScreen(data){
    this.props.navigation.navigate('Modify', {callback: this.callbackFromChildScreen.bind(this), imgUrl: data.imageUrl, name: data.name, description: data.foodDescription});
  }

  render(){
    return(
      <View>
        <MainList listData={this.props.list} itemClick={this.pushEditScreen} />
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TestListScreen)
