export function AddItem(list = [], action){
  if (action.item === undefined){
    return list
  }

  var record = 0
  if (action.type == "modify"){
    for(var i = 0; i < list.length; i++){
      var item = list[i]
      if (item.key == action.item.key){
        record = i
        break
      }
    }
    list[i] = action.item
  }
  else{ // add
    return list.concat([action.item])
  }
}
