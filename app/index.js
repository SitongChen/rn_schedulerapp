import React, {Component} from 'react'
import {Provider} from 'react-redux'
import AppNavigatorContainer from './navigation'
import mainReducer from './reducers'
import { createStore } from 'redux'

let store = createStore(mainReducer)
const unsubscribe = store.subscribe(() =>
  console.log(store.getState())
)

import listData from './features/tmp_list/components/tmpData'
listData.forEach(item =>{
  store.dispatch({type: "add_item", item :item})
})

export default class App extends Component{
  render(){
    return (
      <Provider store={store}>
        <AppNavigatorContainer />
      </Provider>
    )
  }
}
