import TmpList from "../features/tmp_list/containers"
import AddItemScene from "../features/additem/containers"
import {StackNavigator} from 'react-navigation'

const appNavigator = StackNavigator({
    Main: {screen: TmpList},
    Modify: {screen: AddItemScene},
  },
  {
    initialRouteName: 'Main',
  }
);

export default appNavigator;
