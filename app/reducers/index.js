import { combineReducers } from 'redux'
import { AddItem } from '../features/tmp_list/reducers'
import {navReducer} from '../navigation/reducers'
const cbReducers = combineReducers({
  list : AddItem,
  nav : navReducer
})

export default cbReducers;
