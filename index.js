import { AppRegistry } from 'react-native';
import App from './app/index';
// import AppNavigatorContainer from './app/navigation'


// The entry point of the app.
AppRegistry.registerComponent('RN_SchedulerApp', () => App);
